import sqlite3
import logging 

def verifBDD(bdd_entree): # fonction prenant en paramètre la BDD
    logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s') # authentification du logging
    
    connection = sqlite3.connect(bdd_entree) # création de la BDD
    logging.info("Connexion a la base de donnees")
    cursor = connection.cursor() # création du cursor de la BDD

    try: # créé la table avec les colonnes suivantes
        cursor.execute('''CREATE TABLE auto(
        adresse_titulaire text,
        nom text,
        prenom text,
        immatriculation text,
        date_immatriculation text,
        marque text,
        denomination_commerciale text,
        couleur text,
        carrosserie text,
        categorie text,
        cylindree text,
        energie integer,
        places integer,
        poids integer,
        puissance integer,
        type integer,
        variante text,
        version text)
    ''')
        logging.info("La table est creee")
    except sqlite3.OperationalError as e:
        logging.warning(e)
    connection.commit() # met à jour les changements
    connection.close()    # ferme la connexion à la BDD
    return bdd_entree