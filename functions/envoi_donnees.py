import csv
import os
import sqlite3
import logging

def envoi_donnees(csv_entree, bdd_entree):
    connection = sqlite3.connect(bdd_entree, timeout = 10) # connexion à la BDD
    cursor = connection.cursor() # création du cursor de la BDD
    logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s') # création du fichier de logging

    with open(csv_entree) as fichierEntree: # ouverture du fichier csv
        for liste_de_base in list(csv.reader(fichierEntree, delimiter = ';'))[1:]: # parcoure le fichier à partir de la ligne 1
                immat = liste_de_base[3] # initialise l'index de l'immatriculation
                cursor.execute('SELECT immatriculation FROM auto WHERE immatriculation = ?', (immat,)) # instruction SQL vérifiant si l'immat est déjà présente
#               Si l'immat est nouvelle alors il insert la donnée, sinon il met à jour sa nouvelle valeur            #
                try:
                        if(cursor.fetchone() is None):
                                cursor.execute('INSERT INTO auto (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindree, energie, places, poids, puissance, type, variante, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', liste_de_base)
                        else: 
                                liste = liste_de_base
                                liste.append(immat)
                                cursor.execute('UPDATE auto SET adresse_titulaire = ?, nom = ?, prenom = ?, immatriculation = ?, date_immatriculation = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE immatriculation = ?;', liste)   
                except sqlite3.ProgrammingError as e:
                        logging.error("Nombre de données dans le csv est incorrect")
                        logging.info("Le programme ne s'est pas execute correctement")
                        connection.commit()
                        connection.close()
                        logging.info("Fermeture de la BDD...")
                        exit(3)
#                                                                                                                   #
        logging.info("les donnees ont ete ajoutees a la BBD")
        connection.commit() # met à jour les changements
    connection.close() # ferme la connexion à la BDD
    logging.info("Fermeture de la BDD...")