import unittest
from functions.verifBDD import verifBDD
from functions.envoi_donnees import envoi_donnees


class testAuto(unittest.TestCase):
    # test du découpage
    def test_existence_bdd(self):
        test_bdd = "bdd/testbdd"
        self.assertEqual(verifBDD(test_bdd), "bdd/testbdd")

        