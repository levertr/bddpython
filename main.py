import sqlite3
import logging
import os
import csv
import sys
import atexit
import argparse
from functions.verifBDD import verifBDD
from functions.envoi_donnees import envoi_donnees

logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s')
logging.info("Le programme vient d'etre execute...")

if(len(sys.argv) != 3): # test du nombre d'arguments
    logging.warning("Le nombre d'arguments est incorrect")

try: # prend en argument fichier csv et la base de données
    csv_entree = sys.argv[1]
    bdd_entree = sys.argv[2]
except IndexError as e:
    logging.error(e)
    logging.info("Le programme ne s'est pas execute correctement")
    exit(1)

# vérifie la présence du fichier d'entree et ouvre le fichier
try:
    with open(csv_entree, 'r+') as fichierEntree:
        logging.info("Connexion au csv...")
except FileNotFoundError as e:
    logging.error(e)
    logging.info("Le programme ne s'est pas execute correctement")
    exit(2)

verifBDD(bdd_entree) # appelle la fonction se connectant à la BDD
envoi_donnees(csv_entree, bdd_entree) # appelle la fonction envoyant les données à la BDD